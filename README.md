# Composing PC slide deck
https://gitlab.com/sultanahamer/mobile-pc/-/blob/master/Composing%20PC%20Geeknight%20talk.odp

## Setting up termux with all the required apps
Use below link to restore the backup file mentioned below

In this link, goto section `Using termux-restore` and restore the below backup file

https://wiki.termux.com/wiki/Backing_up_Termux


## Backup file
https://gitlab.com/sultanahamer/mobile-pc/-/blob/master/termux_backup.tar.xz

## Configuring Neovim, termux and other utilities

Find the instructions to configure in this provision.sh from https://gitlab.com/sultanahamer/dotfiles/-/blob/termux/provision.sh

## Installing nerd font

Download Jetbrains mono font from https://www.nerdfonts.com/font-downloads
It will be a zip with a ton of styles of same font.
From all those styles, try to find the one with Mono medium medium ttf file and copy it to `~/.termux/font.ttf` inside termux

## Setting up neovim for first time

Open neovim by typing nvim - It will download the plugin manager on first launch and we might see bunch of errors. Press enter to ignore them.
Close neovim by typing `:qa!`

Reopen neovim and type `:PlugInstall` - this will install all the plugins we need.

## Installing tree sitter for c, python

Install neovim treesitter for c and python languages by typing `:TSInstall c python`
Treesitter will help in syntax highlighting for a programming language inside neovim. C and python are examples, we can install
the languages we want to work on

## Raise issue for troubleshooting

In case we encounter some [issue](https://gitlab.com/sultanahamer/mobile-pc/-/issues), raise a issue over here and we can collaborate over the situation

